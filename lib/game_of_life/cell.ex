defmodule GameOfLife.Cell do

  # Responsible for determining if a particular alive cell {x, y} should still be alive on the next generation or not.
  def keep_alive?(alive_cells, {x, y} = _alive_cell) do
    case count_neighbors(alive_cells, x, y, 0) do
          2 -> true
          3 -> true
      _else -> false
    end
  end

  # Rule: Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
  def become_alive?(alive_cells, {x, y} = _dead_cell) do
    3 == count_neighbors(alive_cells, x, y, 0)
  end

  # Find the dead cells of alive neighbors.
  def dead_neighbors(alive_cells) do
    neighbors = neighbors(alive_cells, [])
    (neighbors |> Enum.uniq) -- alive_cells
  end


  ## Private

  defp count_neighbors([], _x, _y, count), do: count

  defp count_neighbors([head_cell | tail_cells], x, y, count) do
    increment = case head_cell do
      {hx, hy} when hx == x - 1 and hy == y - 1 -> 1
      {hx, hy} when hx == x     and hy == y - 1 -> 1
      {hx, hy} when hx == x + 1 and hy == y - 1 -> 1

      {hx, hy} when hx == x - 1 and hy == y     -> 1
      {hx, hy} when hx == x + 1 and hy == y     -> 1

      {hx, hy} when hx == x - 1 and hy == y + 1 -> 1
      {hx, hy} when hx == x     and hy == y + 1 -> 1
      {hx, hy} when hx == x + 1 and hy == y + 1 -> 1

      _not_neighbor -> 0
    end
    count_neighbors(tail_cells, x, y, count + increment)
  end

  defp neighbors([], neighbors), do: neighbors

  defp neighbors([{x, y} | cells], neighbors) do
    neighbors(cells, neighbors ++ [
      {x - 1, y - 1}, {x    , y - 1}, {x + 1, y - 1},
      {x - 1, y    }, {x + 1, y    },
      {x - 1, y + 1}, {x    , y + 1}, {x + 1, y + 1}
    ])
  end

end