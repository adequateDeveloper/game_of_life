defmodule GameOfLife.CellTest do
  use ExUnit.Case, async: true
  alias GameOfLife.Cell

  # Rule: Any live cell with fewer than two live neighbors dies, as if caused by under-population.

  test "alive cell with no neighbors dies" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell]
    refute GameOfLife.Cell.keep_alive?(alive_cells, alive_cell)
  end

  test "alive cell with no neighbors other non-neighbors dies" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {-2, -1}]
    refute Cell.keep_alive?(alive_cells, alive_cell)
  end

  test "alive cell with 1 neighbor dies" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {0, 0}]
    refute Cell.keep_alive?(alive_cells, alive_cell)
  end

  # Rule: Any live cell with more than three live neighbors dies, as if by over-population.

  test "alive cell with more than 3 neighbors dies" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {0, 0}, {1, 0}, {2, 0}, {2, 1}]
    refute Cell.keep_alive?(alive_cells, alive_cell)
  end

  # Rule: Any live cell with two or three live neighbors lives on to the next generation.

  test "alive cell with 2 neighbors lives" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {0, 0}, {1, 0}]
    assert Cell.keep_alive?(alive_cells, alive_cell)
  end

  test "alive cell with 2 neighbors and other non-neighbors lives" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {0, 0}, {-2, -1}, {1, 0}]
    assert Cell.keep_alive?(alive_cells, alive_cell)
  end

  test "alive cell with 3 neighbors lives" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {0, 0}, {1, 0}, {2, 1}]
    assert Cell.keep_alive?(alive_cells, alive_cell)
  end

  test "alive cell with 3 neighbors and other non-neighbors lives" do
    alive_cell = {1, 1}
    alive_cells = [alive_cell, {0, 0}, {-2, -1}, {1, 0}, {2, 1}]
    assert Cell.keep_alive?(alive_cells, alive_cell)
  end

  # Rule: Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

  test "dead cell with three live neighbors becomes a live cell" do
    alive_cells = [{2, 2}, {1, 0}, {2, 1}]
    dead_cell = {1, 1}
    assert Cell.become_alive?(alive_cells, dead_cell)
  end

  test "dead cell with three live neighbors and other non-neighbors becomes a live cell" do
    alive_cells = [{2, 2}, {-2, -1}, {1, 0}, {2, 1}]
    dead_cell = {1, 1}
    assert Cell.become_alive?(alive_cells, dead_cell)
  end

  test "dead cell with two live neighbors stays dead" do
    alive_cells = [{2, 2}, {1, 0}]
    dead_cell = {1, 1}
    refute Cell.become_alive?(alive_cells, dead_cell)
  end

  test "dead cell with two live neighbors and other non-neighbors stays dead" do
    alive_cells = [{2, 2}, {-2, -1}, {1, 0}]
    dead_cell = {1, 1}
    refute Cell.become_alive?(alive_cells, dead_cell)
  end

  test "find the dead cells that are neighbors of the alive cell" do
    alive_cells = [{1, 1}]
    dead_neighbors = Cell.dead_neighbors(alive_cells) |> Enum.sort
    expected_dead_neighbors = [
      {0, 0}, {1, 0}, {2, 0},
      {0, 1}, {2, 1},
      {0, 2}, {1, 2}, {2, 2}
    ] |> Enum.sort
    assert dead_neighbors == expected_dead_neighbors
  end

  test "find the dead cells that are neighbors of alive cells" do
    alive_cells = [{1, 1}, {2, 1}]
    dead_neighbors = Cell.dead_neighbors(alive_cells) |> Enum.sort
    expected_dead_neighbors = [
      {0, 0}, {1, 0}, {2, 0}, {3, 0},
      {0, 1}, {3, 1},
      {0, 2}, {1, 2}, {2, 2}, {3, 2}
    ] |> Enum.sort
    assert dead_neighbors == expected_dead_neighbors
  end

end